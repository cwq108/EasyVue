$.api={
    login(data){
        return axios.post("/api/sys/user/login", data);
    },
    menuList(data){
        return axios.post("/api/sys/menu/list", data);
    },
};