$.define(function () {
    return {
        listToTree: function (oldArr, idKey, pidkey, rid,childrenKey) {
            if (undefined === rid) {
                rid = "0";
            }
            rid=String(rid);
            if (undefined === childrenKey) {
                childrenKey = "children";
            }
            oldArr.forEach(element => {
                element[pidkey]=String(element[pidkey]);
                let pid = element[pidkey];
                if (pid !== rid) {
                    oldArr.forEach(ele => {
                        //当内层循环的ID== 外层循环的pid时，（说明有children），需要往该内层id里建个children并push对应的数组；
                        ele[idKey]=String(ele[idKey]);
                        if (ele[idKey] === pid) {
                            if (!ele[childrenKey]) {
                                ele[childrenKey] = [];
                            }
                            ele[childrenKey].push(element);
                        }
                    });
                }
            });
            //此时的数组是在原基础上补充了children;
            //这一步是过滤，按树展开，将多余的数组剔除；
            oldArr = oldArr.filter(ele => ele[pidkey] === rid);
            return oldArr;
        }
    }
});